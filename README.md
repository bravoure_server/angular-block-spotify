# Bravoure - Block Spotify

## Directive: Code to be added:

        <block-spotify></block-spotify>

## Use

It can be added in the cms under the option of "blocks", and inserting the block-spotify.


### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-block-spotify": "1.0.2"
      }
    }

and the run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
