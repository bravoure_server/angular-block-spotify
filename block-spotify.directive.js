(function () {
    'use strict';

    function blockSpotify (PATH_CONFIG, $analytics) {

        return {
            restrict: "EA",
            replace: true,

            link: function (scope, element, attrs) {

                element.append(scope.iframe);

                element.on('click', '.soundcloud__placeholder', function() {

                    var component = $(this).parents('.soundcloud__container'),
                        placeholder = component.find('.soundcloud__placeholder'),
                        iFrame = scope.processAudioEmbed(scope.module.soundcloud_url, scope.module.size);

                    component.addClass('soundcloud__active');

                    // removes redundant HTML from the DOM
                    placeholder.remove();

                    // adds the iFrame to the DOM
                    component.append(iFrame);

                    $analytics.eventTrack('Click', {
                        category: 'Spotify',
                        action: 'Click',
                        label: scope.processAudioEmbed(scope.module.soundcloud_url, scope.module.size, true)
                    });

                });

            },

            controller: function ($scope, $controller) {

                // Extend controller
                angular.extend(this, $controller('baseController', {
                    $scope: $scope
                }));

                $scope.iframe = $scope.getSpotifyIframe($scope.module.spotify_embed + '&theme=white');

                if (typeof $scope.module.size == 'undefined') {
                    $scope.module.size = 'small';
                }

            },

            templateUrl: PATH_CONFIG.BRAVOURE_COMPONENTS + 'angular-block-spotify/block-spotify.html'

        };

    }

    blockSpotify.$inject = ['PATH_CONFIG', '$analytics'];

    angular
      .module('bravoureAngularApp')
      .directive('blockSpotify', blockSpotify);
})();
